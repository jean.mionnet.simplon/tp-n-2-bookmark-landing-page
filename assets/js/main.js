//Cache la navbar au click//

$('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

//TOGGLER ICON//

var toggler_icon = document.getElementById('nav_button');
var burger_img = document.getElementById('hamburger');

//NAV//
var main_nav = document.getElementById('main_nav');
var links = document.getElementsByTagName('a');

//LOGO//
var logo = document.getElementById('logo');


//NAVBAR ON CLICK//
toggler_icon.onclick = function(){
  if(toggler_icon.classList.contains('collapsed')){
    burger_img.setAttribute('src', 'assets/images/icons/icon-close.svg');
    main_nav.style.backgroundColor = 'rgba(37, 43, 70, 0.9)';
    logo.setAttribute('src', 'assets/images/logo/logo-bookmark-footer.svg')
  }
  else{
    burger_img.setAttribute('src', 'assets/images/icons/icon-hamburger.svg');
    main_nav.style.backgroundColor = '#fff';
    logo.setAttribute('src', 'assets/images/logo/logo-bookmark.svg');
  }
};