Projet n°2 pour Simplon. D'après un challenge de https://www.frontendmentor.io/

Technologies utilisées :

- HTML

- CSS / ANIMATE.CSS / SCSS

- BOOTSTRAP

- JAVASCRIPT

Problèmes connus :

- Pas de message d'erreur dans le formulaire
- Problème de collapse de la navbar, lorsque l'on clique en dehors ou sur un 
  lien.
- Quelques ajustements à faire au niveau des illustrations.